# PassionProject: CRM

This is a passion project that I (Sebastian Hafström) intend to use to learn new things, some of which are:
- Java with Spring and SpringBoot
- Reactive programming (ex. Spring Webflux, Vert.x)
- CI/CD (Jenkins, GitLab, Docker, AWS)
- Terraform
- Microservices

## Project Vision

The long-term vision of this project is to build a tool that have the following features:
- CRM
  - Manage customer and all related to them
- Growth
  - Manage candidates in all stages of the recruitment process.
  - Create JobAds and distribute them
- Consultant Management
  - Track all consultants, their client and other info
- Events
  - Create events hosted by Mpya

## Current Focus

Building a simple, efficient, and effective CRM solution in Java with Spring Boot