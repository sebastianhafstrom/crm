package se.sebastianhafstrom.crm.data.base;

import java.time.LocalDateTime;
import java.util.UUID;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

/**
 * BaseEntity that all entities must extend.
 */
@MappedSuperclass
public class BaseEntity {

  @Id
  @GeneratedValue
  private UUID id;

  @CreatedDate
  @CreationTimestamp
  private LocalDateTime created;

  @LastModifiedDate
  @UpdateTimestamp
  private LocalDateTime modified;

  @Version
  private Long version;

  public UUID getId() {
    return id;
  }

  public LocalDateTime getCreated() {
    return created;
  }
}
