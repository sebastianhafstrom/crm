package se.sebastianhafstrom.crm.data.company.dao;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.sebastianhafstrom.crm.data.company.entity.Company;

@Repository
public interface CompanyRepository extends JpaRepository<Company, UUID> {

}
