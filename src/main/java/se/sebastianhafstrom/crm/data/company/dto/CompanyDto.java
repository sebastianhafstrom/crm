package se.sebastianhafstrom.crm.data.company.dto;

import java.util.UUID;

public class CompanyDto {
  public UUID id;
  public String companyName;
  public String companyNumber;
  public String companyIndustry;
}
