package se.sebastianhafstrom.crm.data.company.entity;

import javax.persistence.Entity;
import se.sebastianhafstrom.crm.data.base.BaseEntity;

@Entity(name = "company")
public class Company extends BaseEntity {

  private String companyName;
  private String companyNumber;
  private String companyIndustry;

  public Company(String companyName, String companyNumber, String companyIndustry) {
    this.companyName = companyName;
    this.companyNumber = companyNumber;
    this.companyIndustry = companyIndustry;
  }

  /**
   * Constructor used by JPA
   */
  protected Company() {

  }

  public String getCompanyName() {
    return companyName;
  }

  public String getCompanyNumber() {
    return companyNumber;
  }

  public String getCompanyIndustry() {
    return companyIndustry;
  }
}
