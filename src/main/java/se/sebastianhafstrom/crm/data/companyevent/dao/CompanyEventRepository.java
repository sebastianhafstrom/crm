package se.sebastianhafstrom.crm.data.companyevent.dao;

import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import se.sebastianhafstrom.crm.data.company.entity.Company;
import se.sebastianhafstrom.crm.data.companyevent.entity.CompanyEvent;

public interface CompanyEventRepository extends JpaRepository<CompanyEvent, UUID> {
  List<CompanyEvent> findAllByCompany(Company company);
}
