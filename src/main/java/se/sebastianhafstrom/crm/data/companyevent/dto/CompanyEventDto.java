package se.sebastianhafstrom.crm.data.companyevent.dto;

import java.time.LocalDateTime;
import java.util.UUID;

public class CompanyEventDto {

  public UUID id;
  public LocalDateTime created;
  public String title;
  public String description;
  public String eventType;
  public UUID companyId;

}
