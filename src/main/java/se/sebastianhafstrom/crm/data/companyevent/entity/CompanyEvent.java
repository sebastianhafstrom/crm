package se.sebastianhafstrom.crm.data.companyevent.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import se.sebastianhafstrom.crm.data.base.BaseEntity;
import se.sebastianhafstrom.crm.data.company.entity.Company;

@Entity
public class CompanyEvent extends BaseEntity {

  private String title;

  private String description;

  @Enumerated(EnumType.STRING)
  private CompanyEventType eventType;

  @ManyToOne
  @JoinColumn(name = "company_id")
  private Company company;

  protected CompanyEvent() {}

  public CompanyEvent(String title, String description, CompanyEventType eventType, Company company) {
    this.title = title;
    this.description = description;
    this.eventType = eventType;
    this.company = company;
  }

  public String getTitle() {
    return title;
  }

  public String getDescription() {
    return description;
  }

  public CompanyEventType getEventType() {
    return eventType;
  }

  public Company getCompany() {
    return company;
  }

  public void setCompany(Company company) {
    this.company = company;
  }

}
