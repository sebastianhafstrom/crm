package se.sebastianhafstrom.crm.data.companyevent.entity;

public enum CompanyEventType {
  EMAIL,
  PHONE_CALL,
  MEETING,
}
