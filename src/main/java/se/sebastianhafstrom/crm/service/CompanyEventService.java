package se.sebastianhafstrom.crm.service;

import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.sebastianhafstrom.crm.data.company.entity.Company;
import se.sebastianhafstrom.crm.data.companyevent.dao.CompanyEventRepository;
import se.sebastianhafstrom.crm.data.companyevent.entity.CompanyEvent;
import se.sebastianhafstrom.crm.data.companyevent.entity.CompanyEventType;

@Service
public class CompanyEventService {

  private final CompanyEventRepository companyEventRepository;
  private final CompanyService companyService;

  @Autowired
  public CompanyEventService(
      CompanyEventRepository companyEventRepository,
      CompanyService companyService) {
    this.companyEventRepository = companyEventRepository;
    this.companyService = companyService;
  }

  public CompanyEvent createCompanyEvent(
      UUID companyId,
      String title,
      String description,
      CompanyEventType eventType
  ) {
    Company company = companyService.getCompany(companyId);
    CompanyEvent companyEvent = new CompanyEvent(title, description, eventType, company);
    return companyEventRepository.save(companyEvent);
  }

  public List<CompanyEvent> getCompanyEvents(UUID companyId) {
    Company company = companyService.getCompany(companyId);
    return getCompanyEvents(company);
  }

  public List<CompanyEvent> getCompanyEvents(Company company) {
    return companyEventRepository.findAllByCompany(company);
  }

}
