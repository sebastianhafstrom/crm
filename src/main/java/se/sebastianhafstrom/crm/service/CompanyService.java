package se.sebastianhafstrom.crm.service;

import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Service;
import se.sebastianhafstrom.crm.data.company.dao.CompanyRepository;
import se.sebastianhafstrom.crm.data.company.entity.Company;

@Service
public class CompanyService {

  private final CompanyRepository companyRepository;

  public CompanyService(CompanyRepository companyRepository) {
    this.companyRepository = companyRepository;
  }

  public Company createCompany(
      String companyName,
      String companyNumber,
      String companyIndustry
  ) {
    Company company = new Company(companyName, companyNumber, companyIndustry);
    return companyRepository.save(company);
  }

  public Company getCompany(UUID id) {
    return companyRepository.findById(id).orElse(null);
  }

  public List<Company> getAllCompanies() {
    return companyRepository.findAll();
  }

}
