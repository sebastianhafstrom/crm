package se.sebastianhafstrom.crm.web;

import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.sebastianhafstrom.crm.data.company.dto.CompanyDto;
import se.sebastianhafstrom.crm.data.company.entity.Company;
import se.sebastianhafstrom.crm.service.CompanyService;

@RestController
@RequestMapping("/company")
public class CompanyController {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  private final CompanyService companyService;

  @Autowired
  public CompanyController(CompanyService companyService) {
    this.companyService = companyService;
  }

  @GetMapping("")
  public ResponseEntity<List<CompanyDto>> getAllCompanies() {
    List<CompanyDto> companyDtos = companyService.getAllCompanies()
        .stream()
        .map(CompanyController::toDto)
        .toList();
    logger.info("Fetching all companies");
    return new ResponseEntity<>(companyDtos, HttpStatus.OK);
  }

  @GetMapping("/{companyId}")
  public ResponseEntity<CompanyDto> getCompany(@PathVariable String companyId) {
    logger.info("Fetching company with id {}", companyId);

    UUID companyUuid;
    try {
      companyUuid = UUID.fromString(companyId);
    } catch (IllegalArgumentException ex) {
      return ResponseEntity.notFound().build();
    }
    Company company = companyService.getCompany(companyUuid);
    logger.info("Fetching company with id {}, name {}, number {}, and industry {}",
        company.getId(),
        company.getCompanyName(),
        company.getCompanyNumber(),
        company.getCompanyIndustry());
    return new ResponseEntity<>(toDto(company), HttpStatus.OK);
  }

  @PostMapping("")
  public ResponseEntity<CompanyDto> createCompany(@RequestBody CreateCompanyRequest request) {
    Company company = companyService.createCompany(
        request.companyName,
        request.companyNumber,
        request.companyIndustry);
    logger.info("Created company with name {}, number {}, and industry {}",
        company.getCompanyName(),
        company.getCompanyNumber(),
        company.getCompanyIndustry());
    return new ResponseEntity<>(toDto(company), HttpStatus.CREATED);
  }

  private static CompanyDto toDto(Company entity) {
    CompanyDto dto = new CompanyDto();
    dto.id = entity.getId();
    dto.companyName = entity.getCompanyName();
    dto.companyNumber = entity.getCompanyNumber();
    dto.companyIndustry = entity.getCompanyIndustry();
    return dto;
  }

  static class CreateCompanyRequest {
    public String companyName;
    public String companyNumber;
    public String companyIndustry;
  }

}
