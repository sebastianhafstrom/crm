package se.sebastianhafstrom.crm.web;

import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.sebastianhafstrom.crm.data.companyevent.dto.CompanyEventDto;
import se.sebastianhafstrom.crm.data.companyevent.entity.CompanyEvent;
import se.sebastianhafstrom.crm.data.companyevent.entity.CompanyEventType;
import se.sebastianhafstrom.crm.service.CompanyEventService;

@RestController
@RequestMapping("/companyEvent")
public class CompanyEventController {

  private final Logger LOG = LoggerFactory.getLogger(getClass());

  private final CompanyEventService companyEventService;

  @Autowired
  public CompanyEventController(CompanyEventService companyEventService) {
    this.companyEventService = companyEventService;
  }

  @PostMapping("")
  public ResponseEntity<CompanyEventDto> createCompanyEvent(
      @RequestBody CreateCompanyEventRequest companyEventRequest) {
    UUID companyUuid;
    try {
      companyUuid = UUID.fromString(companyEventRequest.companyId);
    } catch (IllegalArgumentException ex) {
      return ResponseEntity.notFound().build();
    }

    CompanyEventType companyEventType = CompanyEventType.valueOf(companyEventRequest.eventType);

    CompanyEvent companyEvent = companyEventService.createCompanyEvent(
        companyUuid,
        companyEventRequest.title,
        companyEventRequest.description,
        companyEventType
    );
    LOG.info("Created CompanyEvent\nId {}\nTitle: {}\nDescription: {}\nCompany: {}",
        companyEvent.getId(),
        companyEvent.getTitle(),
        companyEvent.getDescription(),
        companyEvent.getCompany().getId());
    return new ResponseEntity<>(toDto(companyEvent), HttpStatus.CREATED);
  }

  @GetMapping("/{companyId}")
  public ResponseEntity<List<CompanyEventDto>> getCompanyEventsForCompany(
      @PathVariable String companyId) {
    UUID companyUuid;
    try {
      companyUuid = UUID.fromString(companyId);
    } catch (IllegalArgumentException ex) {
      return ResponseEntity.notFound().build();
    }

    List<CompanyEvent> companyEvents = companyEventService.getCompanyEvents(companyUuid);
    List<CompanyEventDto> dtos = companyEvents.stream().map(CompanyEventController::toDto).toList();
    return new ResponseEntity<>(dtos, HttpStatus.OK);
  }

  private static CompanyEventDto toDto(CompanyEvent entity) {
    CompanyEventDto dto = new CompanyEventDto();
    dto.id = entity.getId();
    dto.created = entity.getCreated();
    dto.title = entity.getTitle();
    dto.description = entity.getDescription();
    dto.eventType = entity.getEventType().toString();
    dto.companyId = entity.getCompany().getId();
    return dto;
  }

  static class CreateCompanyEventRequest {
    public String title;
    public String description;
    public String companyId;
    public String eventType;
  }

}
