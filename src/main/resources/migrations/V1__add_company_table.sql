CREATE TABLE company
(
    id               UUID NOT NULL,
    created          TIMESTAMP WITHOUT TIME ZONE,
    modified         TIMESTAMP WITHOUT TIME ZONE,
    version          BIGINT,
    company_name     VARCHAR,
    company_number   VARCHAR,
    company_industry VARCHAR,
    CONSTRAINT pk_company PRIMARY KEY (id)
);