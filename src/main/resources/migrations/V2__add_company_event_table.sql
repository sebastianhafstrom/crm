CREATE TABLE company_event
(
    id          UUID NOT NULL,
    created     TIMESTAMP WITHOUT TIME ZONE,
    modified    TIMESTAMP WITHOUT TIME ZONE,
    version     BIGINT,
    title       VARCHAR(255),
    description VARCHAR(255),
    event_type  VARCHAR(255),
    company_id  UUID,
    CONSTRAINT pk_companyevent PRIMARY KEY (id)
);

ALTER TABLE company_event
    ADD CONSTRAINT FK_COMPANYEVENT_ON_COMPANY FOREIGN KEY (company_id) REFERENCES company (id);